document.getElementById('js-trigger').addEventListener('click', ()=>{
	let menu = document.getElementById('js-menu');
	let hamburger = document.getElementById('js-trigger');
    if (menu.classList.contains("open"))
    	menu.classList.remove('open');
    else 
    	menu.classList.add('open');

    if (hamburger.classList.contains("is-active"))
    	hamburger.classList.remove('is-active');
    else 
    	hamburger.classList.add('is-active');
    
});